import { Application } from "express";
import examplesRouter from "./api/controllers/examples/router";
import newsRouter from "./api/controllers/news/router";
export default function routes(app: Application): void {
  app.use("/api/v1/examples", examplesRouter);
  app.use("/app/v1/news", newsRouter)
}
