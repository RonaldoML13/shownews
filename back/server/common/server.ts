import express from "express";
import { Application } from "express";
import path from "path";
import http from "http";
import os from "os";
import cookieParser from "cookie-parser";
import l from "./logger";
import morgan from "morgan";
import Database, { IDatabase } from "./database";
import { Agenda } from 'agenda/es';
const fetch = require('node-fetch');

import errorHandler from "../api/middlewares/error.handler";
import * as OpenApiValidator from "express-openapi-validator";
import mongoose from "mongoose";

const app = express();

export default class ExpressServer {
  constructor() {
    const root = path.normalize(__dirname + "/../..");
    app.set("appPath", root + "client");
    app.use(morgan("dev"));
    app.use(express.json({ limit: process.env.REQUEST_LIMIT || "100kb" }));
    app.use(
      express.urlencoded({
        extended: true,
        limit: process.env.REQUEST_LIMIT || "100kb",
      })
    );
    app.use(express.text({ limit: process.env.REQUEST_LIMIT || "100kb" }));
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(express.static(`${root}/public`));

    const apiSpec = path.join(__dirname, "api.yml");
    const validateResponses = !!(
      process.env.OPENAPI_ENABLE_RESPONSE_VALIDATION &&
      process.env.OPENAPI_ENABLE_RESPONSE_VALIDATION.toLowerCase() === "true"
    );
    app.use(process.env.OPENAPI_SPEC || "/spec", express.static(apiSpec));
    app.use(
      OpenApiValidator.middleware({
        apiSpec,
        validateResponses,
        ignorePaths: /.*\/spec(\/|$)/,
      })
    );

  }

  router(routes: (app: Application) => void): ExpressServer {
    routes(app);
    app.use(errorHandler);
    return this;
  }

  database(db: IDatabase): ExpressServer {
    db.init();
    return this;
  }

  fetching (connectionString: string): ExpressServer{
    

    var agenda = new Agenda({ db: {address: connectionString} });

    agenda.define('test', async(job, done) => {
        console.log('agenda test');
        const resp = await fetch('https://hn.algolia.com/api/v1/search_by_date?query=nodejs');
        const result = await resp.json();
        console.log(mongoose.connection.collections)
        if(mongoose.connection.collection('news')){
          mongoose.connection.collections.news.insertMany(result.hits);
        }else{
          const cl = await mongoose.connection.createCollection('news');
          cl.insertMany(result.hits);
        }
        
         done();
    });
    agenda.on('ready', () => {
        agenda.every('15 minutes', 'test');
        agenda.start();
    });
    return this
  }

  listen(port: number): Application {
    const welcome = (p: number) => (): void =>
      l.info(
        `up and running in ${
          process.env.NODE_ENV || "development"
        } @: ${os.hostname()} on port: ${p}}`
      );

    http.createServer(app).listen(port, welcome(port));

    return app;
  }
}

