import l from "../../common/logger";
import { New, INewsModel } from '../models/new';

export class NewsService {

    async getAll(): Promise<INewsModel[]> {
        l.info("fetch all news");
        const news: INewsModel[] = await New.find();
        return news; 
    }

    async delete(id: string): Promise<any> {
        console.log(id)
        l.info("Delete new");
        await New.updateOne({_id: id}, {show: false}, { runValidators: true });
        return "deleted";
    }

}

export default new NewsService();