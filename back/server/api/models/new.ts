import mongoose from "mongoose";
import sequence from "mongoose-sequence";

const AutoIncrement = sequence(mongoose);

export interface INewsModel extends mongoose.Document {
    // id: number;
    author: String,
    title: String,
    story_title: String,
    url: String,
    story_url: String,
    created_at: String,
    show: Boolean,
}

const schema = new mongoose.Schema(
  {
    // id: { type: Number, unique: true },
    author: String,
    title: String,
    story_title: String,
    url: String,
    story_url: String,
    created_at: String,
    show: {type: Boolean, default: true},
  },
  {
    collection: "news",
  }
);

// schema.plugin(AutoIncrement, { inc_field: "id" });

export const New = mongoose.model<INewsModel>("New", schema);
