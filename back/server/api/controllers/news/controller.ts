
import newsService from '../../services/news.service';
import { Request, Response, NextFunction } from "express";

export class Controller {
    async getAll(req: Request, res: Response, next: NextFunction) {
        try {
          const docs = await newsService.getAll();
          return res.status(200).json(docs);
        } catch (err) {
          return next(err);
        }
      }
    async delete(req: Request, res: Response, next: NextFunction){
      const id: string = req.params.id;
      try{
        const doc = await newsService.delete(id);
        return res.status(200).json({deleted: true, id});
      }catch(err) {
        return next(err);
      }
    }
}

export default new Controller();